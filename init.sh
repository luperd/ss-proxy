#!/bin/bash

printf "Starting SSR installation. Please make sure you have user-config.json in your root folder \n";

if [ "$1" != "quick" ]; then
    
    sudo DEBIAN_FRONTEND=noninteractive apt-get update;

    UbuntuVersion=`lsb_release -r | awk '{print $2}'`;
    printf "Ubuntu Version: $UbuntuVersion \n";

    if [ "$UbuntuVersion" = "14.04" ]; then
        printf "Ubuntu 16.04 or higher required. Please update manually. \n";
        exit 1;

    elif [ "$UbuntuVersion" = "16.04" ]; then

        IFS='.' read -ra KVARRAY <<< `uname -r`;

        KernelVersion="${KVARRAY[0]}.${KVARRAY[1]}";
        printf "Kernel version: $KernelVersion \n";

        if [ "${KVARRAY[0]}" -lt "4" ] || [ "${KVARRAY[0]}" -eq "4" -a "${KVARRAY[1]}" -le "9" ]; then

            printf "Kernel version $KernelVersion is lower than the recommended version of 4.9. \n  Kernel update required to for google bbr implementation. Proceeding to upgrade ---\n";

            apt-get install --install-recommends linux-generic-hwe-16.04 -y
            # sudo apt-get dist-upgrade --no-install-recommends -y
            
        fi

    fi
else
    printf "Using quick mode. Please make sure:";
    printf "    1) You are running ubuntu 16.04 with Kernel version 4.9 or above.";
    printf "    2) you have user-config.json in your root folder \n";
fi


# Install packages noninteractively
printf "Installing required packages\n";
sudo DEBIAN_FRONTEND=noninteractive apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get install software-properties-common -y
sudo DEBIAN_FRONTEND=noninteractive apt-get install build-essential -y
sudo DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends autoconf libtool libssl-dev libpcre3-dev libev-dev asciidoc xmlto automake -y
sudo DEBIAN_FRONTEND=noninteractive apt-get install python -y


# Increase the limit of file descriptors opened
sudo echo '* soft nofile 51200' >> /etc/security/limits.conf
sudo echo '* hard nofile 51200' >> /etc/security/limits.conf
# Tune the kernel parameters
#   Reuse ports and conections as soon as possible.
#   Enlarge the queues and buffers as large as possible.
#   Choose the TCP congestion algorithm for large latency and high throughput.
sudo echo 'fs.file-max = 51200' >> /etc/sysctl.conf
sudo echo 'net.ipv4.ip_forward = 1' >> /etc/sysctl.conf
sudo echo 'net.ipv6.conf.all.forwarding = 1' >> /etc/sysctl.conf
sudo echo 'net.core.rmem_max = 67108864' >> /etc/sysctl.conf
sudo echo 'net.core.wmem_max = 67108864' >> /etc/sysctl.conf
sudo echo 'net.core.netdev_max_backlog = 250000' >> /etc/sysctl.conf
sudo echo 'net.core.somaxconn = 4096' >> /etc/sysctl.conf
sudo echo 'net.ipv4.tcp_syncookies = 1' >> /etc/sysctl.conf
sudo echo 'net.ipv4.tcp_tw_reuse = 1' >> /etc/sysctl.conf
sudo echo 'net.ipv4.tcp_tw_recycle = 0' >> /etc/sysctl.conf
sudo echo 'net.ipv4.tcp_fin_timeout = 30' >> /etc/sysctl.conf
sudo echo 'net.ipv4.tcp_keepalive_time = 1200' >> /etc/sysctl.conf
sudo echo 'net.ipv4.ip_local_port_range = 10000 65000' >> /etc/sysctl.conf
sudo echo 'net.ipv4.tcp_max_syn_backlog = 8192' >> /etc/sysctl.conf
sudo echo 'net.ipv4.tcp_max_tw_buckets = 5000' >> /etc/sysctl.conf
sudo echo 'net.ipv4.tcp_fastopen = 3' >> /etc/sysctl.conf
sudo echo 'net.ipv4.tcp_mem = 25600 51200 102400' >> /etc/sysctl.conf
sudo echo 'net.ipv4.tcp_rmem = 4096 87380 67108864' >> /etc/sysctl.conf
sudo echo 'net.ipv4.tcp_wmem = 4096 65536 67108864' >> /etc/sysctl.conf
sudo echo 'net.ipv4.tcp_mtu_probing = 1' >> /etc/sysctl.conf
# Enable TCP BBR
sudo echo 'net.core.default_qdisc=fq' >> /etc/sysctl.conf
sudo echo 'net.ipv4.tcp_congestion_control=bbr' >> /etc/sysctl.conf
# Reload sysctl config
sudo sysctl -p

# Setup SSR
mv ~/user-config.json ~/ss-proxy/ss-server/shadowsocksr/user-config.json
cd ~/ss-proxy/ss-server/shadowsocksr
bash initcfg.sh

# Setup reboot crontab
cd ~/ss-proxy
chmod +x ssr-start.sh
echo "@reboot ~/ss-proxy/ssr-start.sh"| crontab -;

if [ "$1" != "quick" ]; then
    # reboot system
    reboot
else
    # start ssr
    cd ~/ss-proxy;
    ./ssr-start.sh;
fi