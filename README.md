# SS-Proxy

Auto setup of ssr. For the deployment of dedicated proxy server. 
Ubuntu 18.04 is recommended. May fail or cause conflicts if not installed on a fresh install of Ubuntu 16.04 or above.

Make sure user-config.json exists in root folder. Template according to user-config.example.json.

### Full Install

**Warning** A full setup will attempt to update kernel if below version 4.9 (needed to enable TCP BBR).

```
bash init.sh
```

### Quick Install

Ubuntu 18.04 is recommended.

```
bash init.sh quick
```