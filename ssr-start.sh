#!/bin/bash

# Increase the limit of file descriptors opened
ulimit -n 51200

nohup python ~/ss-proxy/ss-server/shadowsocksr/shadowsocks/server.py -c ~/ss-proxy/ss-server/shadowsocksr/user-config.json start &>/dev/null &