#!/bin/bash

chmod +x *.sh
chmod +x shadowsocks/*.sh
cp -n apiconfig.py userapiconfig.py
cp -n mysql.json usermysql.json

# Start ShadowsocksR
# cd ~/ss-proxy/ss-server/shadowsocksr/shadowsocks
# nohup python ~/ss-proxy/ss-server/shadowsocksr/shadowsocks/server.py -c ~/ss-proxy/ss-server/shadowsocksr/user-config.json start &>/dev/null &


# nohup python ~/shadowsocksr/shadowsocks/server.py start &>/dev/null &